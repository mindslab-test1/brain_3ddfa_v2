FROM docker.maum.ai:443/brain/vision:v0.2.0-cu101-torch160

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN sh ./build.sh

EXPOSE 6010
