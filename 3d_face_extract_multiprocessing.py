# coding: utf-8

__author__ = 'cleardusk'  # original author
"""
Author: Hyoung-Kyu Song
This code is for extracting 3D faces from multiple videos with multi-processing manner.
It saves 3D features externally with `.obj` extension.
"""
import argparse
from collections import deque
import imageio
import glob
import multiprocessing
import os
from pathlib import Path
import yaml

import numpy as np
import open3d as o3d
from tqdm import tqdm

from FaceBoxes import FaceBoxes
from TDDFA import TDDFA
from utils.render import render
from utils.serialization import get_colors
from utils.functions import cv_draw_landmark, get_suffix


def main(args, path_set, index):
    cfg = yaml.load(open(args.config), Loader=yaml.SafeLoader)

    for video_path in path_set:
        print('Processing video at {}'.format(video_path))

        # Init FaceBoxes and TDDFA, recommend using onnx flag
        if args.onnx:
            os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
            os.environ['OMP_NUM_THREADS'] = '4'

            from FaceBoxes.FaceBoxes_ONNX import FaceBoxes_ONNX
            from TDDFA_ONNX import TDDFA_ONNX

            face_boxes = FaceBoxes_ONNX()
            tddfa = TDDFA_ONNX(**cfg)
        else:
            gpu_mode = args.mode == 'gpu'
            tddfa = TDDFA(gpu_mode=gpu_mode, **cfg)
            face_boxes = FaceBoxes()

        # Given a video path
        fn = video_path.split('/')[-1]
        reader = imageio.get_reader(video_path)

        fps = reader.get_meta_data()['fps']
        suffix = get_suffix(video_path)

        os.makedirs(args.path_save, exist_ok=True)
        video_wfp = os.path.join(args.path_save, f'{fn.replace(suffix, "")}_3d_smooth.mp4')
        writer = imageio.get_writer(video_wfp, fps=fps)

        suffix_ = fn.replace(suffix, "")
        if os.path.exists(Path(video_wfp).parent / f"{suffix_}") is not True:
            os.mkdir(Path(video_wfp).parent / f"{suffix_}")

        # the simple implementation of average smoothing by looking ahead by n_next frames
        # assert the frames of the video >= n
        n_pre, n_next = args.n_pre, args.n_next
        n = n_pre + n_next + 1
        queue_ver = deque()
        queue_frame = deque()

        # run
        pre_ver = None
        for i, frame in tqdm(enumerate(reader)):
            if args.start > 0 and i < args.start:
                continue
            if args.end > 0 and i > args.end:
                break

            frame_bgr = frame[..., ::-1]  # RGB->BGR

            if i == 0:
                # detect
                boxes = face_boxes(frame_bgr)
                boxes = [boxes[0]]
                param_lst, roi_box_lst = tddfa(frame_bgr, boxes)
                ver = tddfa.recon_vers(param_lst, roi_box_lst, dense_flag=True)[0]

                # refine
                param_lst, roi_box_lst = tddfa(frame_bgr, [ver], crop_policy='landmark')
                ver = tddfa.recon_vers(param_lst, roi_box_lst, dense_flag=True)[0]

                # padding queue
                for _ in range(n_pre):
                    queue_ver.append(ver.copy())
                queue_frame.append(frame_bgr.copy())
                queue_ver.append(ver.copy())
                queue_frame.append(frame_bgr.copy())

            else:
                param_lst, roi_box_lst = tddfa(frame_bgr, [pre_ver], crop_policy='landmark')

                roi_box = roi_box_lst[0]
                # todo: add confidence threshold to judge the tracking is failed
                if abs(roi_box[2] - roi_box[0]) * abs(roi_box[3] - roi_box[1]) < 2020:
                    boxes = face_boxes(frame_bgr)
                    boxes = [boxes[0]]
                    param_lst, roi_box_lst = tddfa(frame_bgr, boxes)

                ver = tddfa.recon_vers(param_lst, roi_box_lst, dense_flag=True)[0]

                queue_ver.append(ver.copy())
                queue_frame.append(frame_bgr.copy())

            pre_ver = ver  # for tracking

            # smoothing: enqueue and dequeue ops
            if len(queue_ver) >= n:
                ver_ave = np.mean(queue_ver, axis=0)
                suffix_ = fn.replace(suffix, "")
                wfp = Path(video_wfp).parent / f"{suffix_}/{i:05d}.obj"
                frame_bgr = queue_frame[n_pre]
                save_with_o3d(frame_bgr, [ver_ave], tddfa.tri, height=frame_bgr.shape[0], wfp=str(wfp))

                img_draw = render(frame_bgr, [ver_ave], tddfa.tri, alpha=0.7)
                writer.append_data(img_draw[:, :, ::-1])  # BGR->RGB

                queue_ver.popleft()
                queue_frame.popleft()

        # we will lost the last n_next frames, still padding
        for _ in range(n_next):
            queue_ver.append(ver.copy())
            queue_frame.append(frame_bgr.copy())  # the last frame

            ver_ave = np.mean(queue_ver, axis=0)
            suffix_ = fn.replace(suffix, "")
            wfp = Path(video_wfp).parent / f"{suffix_}/{(i + 1):05d}.obj"
            frame_bgr = queue_frame[n_pre]
            save_with_o3d(frame_bgr, [ver_ave], tddfa.tri, height=frame_bgr.shape[0], wfp=str(wfp))

            img_draw = render(frame_bgr, [ver_ave], tddfa.tri, alpha=0.7)
            writer.append_data(img_draw[..., ::-1])  # BGR->RGB

            queue_ver.popleft()
            queue_frame.popleft()

        writer.close()
        print(f'Dump to {video_wfp}; worker {index}')


def save_with_o3d(img, ver_lst, tri, height, wfp):
    n_obj = len(ver_lst)
    if n_obj <= 0:
        return

    # print(ver_lst[0].shape, tri.shape) #(3, v), (f, 3)
    n_vertex = ver_lst[0].shape[1]

    for i in range(n_obj):
        ver = ver_lst[i].copy()
        colors = get_colors(img, ver)
        offset = i * n_vertex

        colors = colors[..., ::-1]
        ver = ver.transpose(1, 0)
        ver[:, 1] = height - ver[:, 1]
        tri = tri.copy()[..., ::-1] + offset

        o3d_obj = o3d.geometry.TriangleMesh()
        o3d_obj.vertices = o3d.utility.Vector3dVector(ver)
        o3d_obj.triangles = o3d.utility.Vector3iVector(tri)
        o3d_obj.vertex_colors = o3d.utility.Vector3dVector(colors)
        o3d.io.write_triangle_mesh(wfp, o3d_obj)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='The smooth demo of video of 3DDFA_V2')
    parser.add_argument('-c', '--config', type=str, default='configs/mb1_120x120.yml')
    parser.add_argument('-m', '--mode', default='cpu', type=str, help='gpu or cpu mode')
    parser.add_argument('-n_pre', default=1, type=int, help='the pre frames of smoothing')
    parser.add_argument('-n_next', default=1, type=int, help='the next frames of smoothing')
    parser.add_argument('-s', '--start', default=-1, type=int, help='the started frames')
    parser.add_argument('-e', '--end', default=-1, type=int, help='the end frame')
    parser.add_argument('-w', '--num_workers', default=4, type=int, help='Number of workers for given video paths.')
    parser.add_argument('--onnx', action='store_true', default=False)

    parser.add_argument('--path_save', type=str, default='./results', )
    parser.add_argument('--path_glob', type=str, required=True, help='input path glob, example: "./videos/*.mp4"') # video path

    args = parser.parse_args()

    file_paths = sorted(glob.glob(args.path_glob))
    if len(file_paths) == 0:
        print('No matching path exists')
        pass
    # if given a single video path
    elif len(file_paths) == 1:
        main(args, file_paths, 0)
    else:
        num_files = len(file_paths)
        print(f'There are {num_files} video files to be processed.')
        # split file path list and assign to a new process
        for i in range(0, num_files, num_files // args.num_workers):
            path_set = file_paths[i: i + num_files // args.num_workers]
            process = multiprocessing.Process(target=main, args=(args, path_set, i / num_files // args.num_workers))
            process.start()

            print('Process {} is alive:'.format(i), process.is_alive())
